# AppleSystemStatuses

<b>Precondition:</b>

python 3.7.3
instaled libs: flask, urlopen, BeautifulSoup4, pywin32, configparser

<i>There are common error for pywin32: https://stackoverflow.com/questions/41200068/python-windows-service-error-starting-service-the-service-did-not-respond-to-t</i>

<b>To run service:</b>
1. Run RunService.py --username <domain\username> --password <password> install
*Note that username and password are case sensitivе!*
2. Run RunService.py start

Service will be started with default parameters - collect statuses every hour.
You can change it in config.ini by setting new value to TimesPerDay.

<b>To view results</b> run RunHtml.py. 

It will start local html service to view system statuses as html page.

<b>Possible errors:</b>
1. Server don't starts with message "Error starting service: The service did not respond to the start or control request in a timely fashion.". Described in:
https://stackoverflow.com/questions/41200068/python-windows-service-error-starting-service-the-service-did-not-respond-to-t

2. The service did not start due to a logon failure. - Check entered username and password. You can update them through call "RunService.py --username <domain\username> --password <password> update" or manually in Services app.