import sqlite3, logging, os

def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Exception as e:
        print(e)

    return None

def create_table(conn, create_table_sql):
    """ create a table from the create_table_sql statement
    :param conn: Connection object
    :param create_table_sql: a CREATE TABLE statement
    :return:
    """
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Exception as e:
        logging.error(e)


def main():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    logging.basicConfig(filename=dir_path + "\\sysstat.log", level=logging.DEBUG)
    logging.debug("CreateTables started")
    database = dir_path + "\\SystemStatuses.sqlite"

    sql_create_region_table = """ CREATE TABLE IF NOT EXISTS Region (
                                   id INTEGER PRIMARY KEY AUTOINCREMENT,
                                   name TEXT NOT NULL,
                                   href TEXT NOT NULL,
                                   locale TEXT NOT NULL
                               ); """

    sql_create_statusname_table = """CREATE TABLE IF NOT EXISTS StatusName (
                                      id INTEGER PRIMARY KEY AUTOINCREMENT,
                                      region_id INTEGER REFERENCES Region(id),
                                      name TEXT NOT NULL
                                   );"""

    sql_create_updatedate_table = """CREATE TABLE IF NOT EXISTS UpdateDate (
                                      id INTEGER PRIMARY KEY AUTOINCREMENT,
                                      date INTEGER NOT NULL
                                   );"""

    sql_create_systemstatus_table = """CREATE TABLE IF NOT EXISTS SystemStatus (
                                        id INTEGER PRIMARY KEY AUTOINCREMENT,
                                        statusname_id INTEGER REFERENCES StatusName(id),
                                        updatedate_id INTEGER REFERENCES UpdateDate(id),
                                        has_events INTEGER DEFAULT(0),
                                        UNIQUE (statusname_id, updatedate_id)
                                     );"""


    sql_create_statustype_table = """CREATE TABLE IF NOT EXISTS StatusType (
                                      id INTEGER PRIMARY KEY AUTOINCREMENT,
                                      name TEXT NOT NULL
                                   );"""

    sql_create_eventstatus_table = """CREATE TABLE IF NOT EXISTS EventStatus (
                                      id INTEGER PRIMARY KEY AUTOINCREMENT,
                                      name TEXT NOT NULL
                                   );"""

    sql_create_message_table = """CREATE TABLE IF NOT EXISTS Message (
                                      region_id INTEGER REFERENCES Region(id),
                                      message_id INTEGER,
                                      text TEXT NOT NULL,
                                      PRIMARY KEY (region_id, message_id)
                                   );"""

    sql_create_event_table = """CREATE TABLE IF NOT EXISTS Event (
                                   id INTEGER PRIMARY KEY AUTOINCREMENT,
                                   systemstatus_id INTEGER NOT NULL REFERENCES SystemStatus(id),
                                   start_date INTEGER,
                                   end_date INTEGER,
                                   statustype_id INTEGER REFERENCES StatusType(id),
                                   eventstatus_id INTEGER REFERENCES EventStatus(id),
                                   message_id INTEGER REFERENCES Message(message_id)
                                );"""

    conn = create_connection(database)
    if conn is not None:
        create_table(conn, sql_create_region_table)
        create_table(conn, sql_create_statusname_table)
        create_table(conn, sql_create_updatedate_table)
        create_table(conn, sql_create_systemstatus_table)
        create_table(conn, sql_create_statustype_table)
        create_table(conn, sql_create_eventstatus_table)
        create_table(conn, sql_create_message_table)
        create_table(conn, sql_create_event_table)
    else:
        logging.error("Error! cannot create the database connection.")

if __name__ == '__main__':
    main()