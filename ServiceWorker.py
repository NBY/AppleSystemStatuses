import CreateTables, GetRegions, SystemStatuses

def main():
    CreateTables.main()
    GetRegions.main()
    SystemStatuses.main()

if __name__ == '__main__':
    main()