import os, time, configparser, logging
import win32serviceutil, win32service, win32event
import servicemanager, socket


class AppServerSvc(win32serviceutil.ServiceFramework):
    _svc_name_ = "AppleSystemStatusesService"
    _svc_display_name_ = "Apple System Statuses"

    def __init__(self, args):
        win32serviceutil.ServiceFramework.__init__(self, args)
        self.hWaitStop = win32event.CreateEvent(None, 0, 0, None)
        socket.setdefaulttimeout(60)

    def SvcStop(self):
        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
        win32event.SetEvent(self.hWaitStop)

    def SvcDoRun(self):
        self.ReportServiceStatus(win32service.SERVICE_START_PENDING)
        self.ReportServiceStatus(win32service.SERVICE_RUNNING)
        servicemanager.LogMsg(servicemanager.EVENTLOG_INFORMATION_TYPE,
                              servicemanager.PYS_SERVICE_STARTED,
                              (self._svc_name_, ''))
        runServices(self.hWaitStop)


def getSleepTime(dir_path):
    ms_in_hour = 3600000
    ms_in_day = ms_in_hour * 24
    config = configparser.ConfigParser()
    config.read(dir_path + '\\config.ini')
    if 'TimesPerDay' in config['DEFAULT']:
        return ms_in_day / int(config['DEFAULT']['TimesPerDay'])
    return ms_in_hour


def runServices(hWaitStop):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    logging.basicConfig(filename=dir_path + "\\sysstat.log", level=logging.DEBUG)
    logging.info('Service has been started')
    timeout = 0
    while True:
        rc = win32event.WaitForSingleObject(hWaitStop, timeout)
        if rc == win32event.WAIT_OBJECT_0:
            servicemanager.LogInfoMsg("Service has been stopped")
            logging.info('Service has been stopped')
            break
        logging.info('Starting scripts...')
        os.system("python " + dir_path + "\\ServiceWorker.py")
        timeout = getSleepTime(dir_path)
        logging.info('Next update in {0} ms...'.format(timeout))


if __name__ == '__main__':
    win32serviceutil.HandleCommandLine(AppServerSvc)
