from urllib.request import urlopen
import json, sqlite3, time, os, logging


def getField(jsondata, fieldname):
    if fieldname in jsondata:
        return jsondata[fieldname]
    return ""

def getDate(jsondata, fieldname):
    date = getField(jsondata, fieldname)
    if date:
        return date
    return 0

def getEventStatus(jsondata):
    eventslist = list()
    eventsjson = getField(jsondata, "events")
    for eventjson in eventsjson:
        event = dict()
        event["statusType"] = getField(eventjson, "statusType")
        event["eventStatus"] = getField(eventjson, "eventStatus")
        event["epochStartDate"] = getDate(eventjson, "epochStartDate")
        event["epochEndDate"] = getDate(eventjson, "epochEndDate")
        event["messageId"] = getField(eventjson, "messageId")
        event["message"] = getField(eventjson, "message")
        eventslist.append(event)
    return eventslist


def getJsonData(binaryJson):
    try:
        text = binaryJson.decode("utf-8")
        jsonCallback = "jsonCallback("
        if text.startswith(jsonCallback):
            text = text[len(jsonCallback):-2]
        return json.loads(text)
    except Exception as e:
        print("--Failed parsing json:", e)
    return ""


def getSystemStatuses(url):
    systemStatuses = []
    try:
        page = urlopen(url)
        textlines = page.readlines()
        if not textlines:
            print("No data for url")
        else:
            jsondata = getJsonData(textlines[0])
            if "services" in jsondata:
                for service in jsondata["services"]:
                    systemStatus = [getField(service, "serviceName"), getEventStatus(service)]
                    if systemStatus[0] != "":
                        systemStatuses.append(systemStatus)
                    else:
                        print('Service not found')
    except Exception as e:
        print("--Error occured:", e)
    return systemStatuses

def getIdForValue(dbname, cursor, valuelist, value):
    value_id = [item[0] for item in valuelist if item[1] == value]
    if not value_id:
        query = 'INSERT INTO {0} (name) VALUES ("{1}");'.format(dbname, value)
        cursor.execute(query)
        value_id = cursor.lastrowid
        valuelist.append((value_id, value))
        return value_id
    return value_id[0]

def main():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    logging.basicConfig(filename=dir_path + "\\sysstat.log", level=logging.DEBUG)
    logging.info('SystemStatuses started')
    try:
        conn = sqlite3.connect(dir_path + "\\SystemStatuses.sqlite")

        cur = conn.cursor()
        cur.execute("SELECT * FROM StatusType")
        status_types = cur.fetchall()

        cur.execute("SELECT * FROM EventStatus")
        events_statuses = cur.fetchall()

        cur.execute('INSERT INTO UpdateDate (date) VALUES ("{0}");'.format(time.time()))
        updatedate_id = cur.lastrowid

        cur.execute("SELECT id, locale FROM Region")
        regions = cur.fetchall()
        for region in regions:
            logging.debug('<Getting System statuses {0}>'.format(region[1]))
            systemStatuses = getSystemStatuses('https://www.apple.com/support/'
                                               'systemstatus/data/system_status_{0}.js'.format(region[1]))
            # print('<Developer system statuses {0}>:'.format(region[1]))
            # printSystemStatuses('https://www.apple.com/support/systemstatus/data/developer/system_status_{0}.js'.format(region[1]))

            for systemSatatus in systemStatuses:
                try:
                    has_events = 1
                    if not systemSatatus[1]:
                        has_events = 0

                    query = '''INSERT OR REPLACE INTO StatusName (id, region_id, name) VALUES 
                            ((SELECT id FROM StatusName WHERE name = "{1}" AND region_id = {0}), 
                            {0}, "{1}");'''.format(region[0], systemSatatus[0])
                    cur.execute(query)
                    statusname_id = cur.lastrowid

                    query = '''INSERT INTO SystemStatus (statusname_id, updatedate_id, has_events)  
                            VALUES ({0}, {1}, {2});'''.format(statusname_id, updatedate_id, has_events)
                    cur.execute(query)
                    systemstatus_id = cur.lastrowid

                    if has_events == 1:
                        logging.debug("Processing events")
                        for event in systemSatatus[1]:
                            event["statusType"] = getIdForValue("StatusType", cur, status_types, event["statusType"])
                            event["eventStatus"] = getIdForValue("EventStatus", cur, events_statuses, event["eventStatus"])

                            query = 'INSERT OR IGNORE INTO Message (region_id, message_id, text) VALUES ({0}, {1}, "{2}")' \
                                    ''.format(region[0], event["messageId"], event["message"])
                            cur.execute(query)

                            query = '''INSERT INTO Event (systemstatus_id, statustype_id, 
                                    eventstatus_id, start_date, end_date, message_id) 
                                    VALUES ({0}, {1}, {2}, {3}, {4}, {5});'''.format(systemstatus_id,
                                     event["statusType"], event["eventStatus"], event["epochStartDate"],
                                     event["epochEndDate"], event["messageId"])
                            cur.execute(query)
                except Exception as e:
                    logging.error('Error occured for region {0}. Error: {1}'.format(region[1], e))
        conn.commit()
    except Exception as e:
        logging.error(e)

if __name__ == '__main__':
    main()