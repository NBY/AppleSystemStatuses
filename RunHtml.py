from flask import Flask, render_template, request
from datetime import datetime
import sqlite3
app = Flask(__name__)

@app.route('/')
def index():
    conn = sqlite3.connect("SystemStatuses.sqlite")

    cur = conn.cursor()
    cur.execute("SELECT * FROM Region")
    regions = cur.fetchall()
    return render_template('index.html', regions=regions)

@app.route('/UpdateDate/<regionid>')
def UpdateDate(regionid):
    conn = sqlite3.connect("SystemStatuses.sqlite")

    cur = conn.cursor()
    cur.execute("SELECT * FROM UpdateDate")
    updDates = cur.fetchall()
    updateDates = []
    for updDate in updDates:
        updateDates.append([updDate[0], datetime.fromtimestamp(updDate[1])])
    return render_template('UpdateDate.html', region=regionid, updateDates=updateDates)

@app.route('/SystemStatus', methods=['GET'])
def SystemStatus():
    region = request.args.get('region')
    updDate = request.args.get('updDate')

    conn = sqlite3.connect("SystemStatuses.sqlite")

    cur = conn.cursor()
    cur.execute('''SELECT SystemStatus.id, StatusName.name, has_events 
                FROM SystemStatus INNER JOIN StatusName ON SystemStatus.statusname_id = StatusName.id 
                WHERE region_id = {0} AND updatedate_id = {1}'''.format(region, updDate))
    sysStat = cur.fetchall()
    system_statuses = []
    for systemStatus in sysStat:
        cur.execute('''SELECT StatusType.name, EventStatus.name, text, start_date, end_date
                    FROM ((Event INNER JOIN StatusType ON Event.statustype_id = StatusType.id) 
                    INNER JOIN EventStatus ON Event.eventstatus_id = EventStatus.id) 
                    INNER JOIN Message ON Event.message_id = Message.message_id 
                    WHERE systemStatus_id = {0} AND region_id = {1}'''.format(systemStatus[0], region))
        message = cur.fetchall()
        messages = []
        for msg in message:
            messages.append([msg[0], msg[1], msg[2], datetime.fromtimestamp(msg[3]/1000 + 315964), datetime.fromtimestamp(msg[4]/1000  + 315964)])
        status = [systemStatus[1], systemStatus[2], messages]
        system_statuses.append(status)
    return render_template('SystemStatus.html', systemStatuses=system_statuses)

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8000, debug=False)