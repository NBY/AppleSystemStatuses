import sqlite3, logging, configparser, os
from urllib.request import urlopen
from urllib.request import HTTPError
from bs4 import BeautifulSoup

def getLocale(href):
    try:
        locale_url = 'https://www.apple.com' + href + 'support/systemstatus/localizer.js'
        page = urlopen(locale_url)
        pagelines = page.readlines()
        for line in pagelines:
             if b'locale' in line:
                 start = line.find(b'"') + 1
                 end = line.find(b'"', start)
                 return line[start:end].decode('utf-8')
    except HTTPError as e:
        logging.debug(e)
        pass
    except Exception as e:
        logging.error(e)
    return None

def parseSoup(soup):
    regions = []
    if soup:
        try:
            for ul in soup.find_all('ul', class_='countrylist'):
                if not ul:
                    break
                for li in ul.find_all('li'):
                    if not li:
                        break
                    a = li.find('a')
                    if a:
                        region = [a['href']]
                        span = a.find('span')
                        if span:
                            lispan = li.find('span')
                            if lispan:
                                region.append(lispan.text)
                        region.append(getLocale(region[0]))
                        if len(region) == 3 and region[2]:
                            regions.append(region)
        except Exception as e:
            logging.error(e)
    return regions


def main():
    try:
        dir_path = os.path.dirname(os.path.realpath(__file__))
        logging.basicConfig(filename=dir_path + "\\sysstat.log", level=logging.DEBUG)
        logging.debug("GetRegions started")
        config = configparser.ConfigParser()
        config.read(dir_path + "\\config.ini")

        conn = sqlite3.connect(dir_path + "\\SystemStatuses.sqlite")
        cur = conn.cursor()
        cur.execute('SELECT COUNT(*) FROM Region')
        region_count = cur.fetchall()
        update_regions = true if not region_count else region_count[0][0] == 0
        if not update_regions and 'UpdateRegions' in config['DEFAULT']:
            logging.debug("Regions was found. Checking update flag...")
            update_regions = config['DEFAULT']['UpdateRegions'] == "true"

        if update_regions:
            logging.debug("Updating region list...")
            page = urlopen('https://www.apple.com/choose-country-region/')
            soup = BeautifulSoup(page, "html.parser")
            regions = parseSoup(soup)

            for region in regions:
                query = 'INSERT OR REPLACE INTO Region (id, name, href, locale) VALUES '\
                        '((SELECT id FROM Region WHERE locale = "{2}"), '\
                        '"{0}", "{1}", "{2}");'.format(region[1], region[0], region[2])
                cur.execute(query)
            conn.commit()
    except Exception as e:
        logging.error(e)


if __name__ == '__main__':
    main()